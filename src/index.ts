/**
 * BEGIN HEADER
 *
 * Contains:        Utility class
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This file contains functionality to generate regular
 *                  expressions to be used across the application for
 *                  consistent behaviour.
 *
 *                  Originally borrowed from Zettlr.
 *
 * END HEADER
 */

/**
 * Returns a regular expression that matches JSON Pointer Notation.
 *
 * @param   {boolean}  multiline  Whether or not the regular expression should be multiline
 * @returns  {RegExp}             The requested regular expression.
 */
export function getJSONPointerRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''
  return RegExp(
    /(#)(\/.*\/)(.*)/.source,
    // Necessary flags + optional multiline flag
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches Windows, POSIX, and UNC paths as well as http(s) URIs.
 *
 * @param   {boolean}  multiline  Whether or not the regular expression should be multiline
 * @returns  {RegExp}             The requested regular expression.
 */
export function getFilePathRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''
  //                    1                        2           3
  //(/ or [A-Z]: or ./ or ../ or .\ or ..\)(path/to/file)(fileExt)
  return RegExp(
    /(\/|\.\/|\.\.\/|\.\\|\.\.\\|[a-zA-Z]{1}[:\\|/://]|\\\\|http[s]?:)([a-zA-Z0-9_/\-\\.]*)(\.[a-zA-Z0-9]+)/.source,
    // Necessary flags + optional multiline flag
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches if a path is relative (.\ || ..\ || ./ || ../).
 *
 * @param   {boolean}  multiline  Whether or not the regular expression should be multiline
 * @returns  {RegExp}             The requested regular expression.
 */
export function getRelativePathRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''

  return RegExp(
    /\/|\.\/|\.\.\/|\.\\|\.\.\\/.source,
    // Necessary flags + optional multiline flag
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches $ref: /path/to/file style references as strings.
 *
 * @param   {boolean}  multiline  Whether or not the regular expression should be multiline
 * @returns  {RegExp}             The requested regular expression.
 */
export function getStringRefsRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''
  //     1                        2                       3            4
  // ($ref: )(/ or [A-Z]: or ./ or ../ or .\ or ..\)(path/to\file)(.fileExt)
  return RegExp(
    /(\$ref:\s)(\/|\.\/|\.\.\/|\.\\|\.\.\\|[a-zA-Z]{1}[:\\|/://]|\\\\|http[s]?:)([a-zA-Z0-9_/\-\\.]*)(\.[a-zA-Z0-9]+)/
      .source,
    // Necessary flags + optional multiline flag
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches block Maths.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getBlockMathRE(): RegExp {
  return RegExp(/^\s*\$\$\s*$/.source)
}

/**
 * Returns a regular expression that matches Markdown Blocks (???).
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getBlockRE(): RegExp {
  return RegExp(/^(\s*?)(?:#{1,6}|>|\*|\+|-|\d{1,5}\.)(?:\s+)(.*)$/.source)
}

/**
 * Returns a regular expression that matches Markdown Citations.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getCitationRE(): RegExp {
  return RegExp(/(\[(?:[^[\]]*@[^[\]]+)\])|(?<=\s|^)(@[\p{L}\d_][\p{L}\d_:.#$%&\-+?<>~/]*)/.source, 'gu')
}

/**
 * Returns a regular expression that matches Markdown Code Blocks.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getCodeBlockRE(): RegExp {
  return RegExp(/^\s{0,3}(`{3,}|~{3,})/.source)
}

/**
 * Returns a regular expression that matches Markdown inline code.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getCodeRE(): RegExp {
  return RegExp(/`.*?`/.source, 'i')
}

/**
 * Returns a regular expression that matches footnotes for project export.
 *
 * @param   {boolean}  multiline  Whether to match multiline
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getFnExportRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''

  return RegExp(
    /\[\^([\w]+?)\]/.source,
    // Necessary flags + optional multiline flag
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches Markdown footnote references.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getFnRE(): RegExp {
  return RegExp(/(\[\^([\da-zA-Z_-]+)\][^:]|\[\^([\da-zA-Z_-]+)\]$)/.source, 'g')
}

/**
 * Returns a regular expression that matches Markdown footnote references.
 *
 * @param   {boolean}  multiline  Whether to match multiline
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getFnRefRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''

  return RegExp(/^\[\^([\da-zA-Z_-]+)\]: (.+)/.source, 'g' + flag)
}

/**
 * Returns a regular expression that matches Markdown footnote references.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getFnReferenceRE(): RegExp {
  return RegExp(/^\[\^.+\]:\s/.source)
}

/**
 * Returns a regular expression that matches Markdown footnote references.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getFootnoteRefRE(): RegExp {
  return RegExp(/\[\^[^\]]+\]/.source, 'i')
}

/**
 * Returns a regular expression that matches Markdown Headings.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getHeadRE(): RegExp {
  return RegExp(/^(#{1,6}) (.*)/.source, 'g')
}

/**
 * Returns a regular expression that matches Markdown Headings.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getHeadingRE(): RegExp {
  return RegExp(/(#+)\s+/.source)
}

/**
 * Returns a regular expression that matches Markdown highlighting (???).
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getHighlightRE(): RegExp {
  return RegExp(/::.+?::|==.+?==/.source)
}

/**
 * Returns a regular expression that matches file IDs as in the settings
 *
 * @param   {boolean}  multiline  Whether to match multiline
 * @returns  {RegExp}              The compiled Regular Expression
 */
/* export function getIDRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''
  // eslint-disable-next-line @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-assignment
  let idRegExpString = global.config.get('zkn.idRE')
  // Make sure the ID definitely has at least one
  // Capturing group to not produce errors.
  if (!/\(.+?\)/.test(idRegExpString)): RegExp {
    idRegExpString = `(${idRegExpString})`
  }

  return RegExp(idRegExpString, 'g' + flag)
}
*/
/**
 * Returns a regular expression that matches inline frames.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getIframeRE(): RegExp {
  return RegExp(/^<iframe.*?>.*?<\/iframe>$/.source, 'i')
}

/**
 * Returns a regular expression that matches image file names
 *
 * @param   {boolean}  multiline  Whether the expression should match multilines
 * @returns  {RegExp}             The compiled expression
 */
export function getImageFileRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''

  return RegExp(
    /(\.jpg|\.jpeg|\.png|\.gif|\.svg|\.tiff|\.tif)$/.source,
    // Necessary flags + optional multiline flag
    'i' + flag
  )
}

/**
 * Returns a regular expression that can detect Markdown images globally
 *
 * @param   {boolean}  multiline  Whether or not the regular expression should be multiline
 * @returns  {RegExp}           The wanted regular expression.
 */
export function getImageRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''

  return RegExp(
    /(?<=\s|^)!\[(.*?)\]\((.+?(?:(?<= )"(.+)")?)\)({[^{]+})?/.source,
    // Necessary flags + optional multiline flag
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches inline Maths.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getInlineMathRE(): RegExp {
  return RegExp(/^(?:\${1,2}[^\s\\]\${1,2}(?!\d)|\${1,2}[^\s].*?[^\s\\]\${1,2}(?!\d))/.source)
}

/**
 * Returns a regular expression that matches Markdown links.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getLinkRE(): RegExp {
  return RegExp(/^.+\.[a-z0-9]+/.source, 'i')
}

/**
 * Returns a regular expression that matches empty Markdown lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListEmptyRE(): RegExp {
  return RegExp(/^(\s*)([*+-] \[[x ]\]|[*+-]|(\d+)[.)])(\s*)$/.source)
}

/**
 * Returns a regular expression that matches Markdown Ordered lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListOrderedRE(): RegExp {
  return RegExp(/^(\s*)((\d+)([.)]))(\s*)/.source)
}

/**
 * Returns a regular expression that matches Markdown lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListRE(): RegExp {
  return RegExp(/^(\s*)([*+-] \[[x ]\]\s|[*+-]\s|(\d+)([.)]))(\s*)/.source)
}

/**
 * Returns a regular expression that matches Markdown task lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListTaskListRE(): RegExp {
  return RegExp(/^(\s*)(- \[[x ]\])(\s*)/.source)
}

/**
 * Returns a regular expression that matches unordered Markdown lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListTokenRE(): RegExp {
  return RegExp(/^(\s*)(>[> ]*|[*+-] \[[x ]\]|[*+-]|(\d+)[.)])(\s*)$/.source)
}

/**
 * Returns a regular expression that matches unordered Markdown lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListUnorderedCMRE(): RegExp {
  return RegExp(/^(\s*)([*+-])\s/.source)
}

/**
 * Returns a regular expression that matches unordered Markdown lists.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getListUnorderedRE(): RegExp {
  return RegExp(/[*+-]\s/.source)
}

/**
 * Returns a regular expression that matches Markdown files.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getMarkdownFileRE(): RegExp {
  return RegExp(/.+\.(?:md|Markdown|txt)$/.source, 'i')
}

/**
 * Returns a regular expression that matches URL protocols (e.g. http://)
 *
 * @param   {boolean}  multiline  Whether or not the expression should be multiline
 * @returns  {RegExp}           The wanted regular expression
 */
export function getProtocolRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''

  return RegExp(/^([a-z]{1,10}):\/\//, 'i' + flag)
}

/**
 * Returns a regular expression that reference style image links
 *
 * @param   {boolean}  multiline  Whether or not the expression should be multiline
 * @returns  {RegExp}           The wanted regular expression
 */
export function getRefImageRE(multiline = false): RegExp {
  const flag = multiline ? 'm' : ''
  return RegExp(
    /^\[(?!\^)(.*)\]: <?((?:[a-zA-Z]:|\\\\[^/\\:*?"<>|]+\\[^/\\:*?"<>|]+|\.{1,2})(?:\\[^/\\:*?"<>|]+)+(?:\.[^/\\:*?"<>| ]+)|(?:\.{1,2}|~){0,1}(?:(?:\/|\\)(?:[a-zA-Z0-9._-])*)+|\w+:(?:\/?\/?)[^\s>]+)>?(?:\n? +["'(](.*)["')])?(?: \{(.*)\})?/
      .source,
    'g' + flag
  )
}

/**
 * Returns a regular expression that matches inline frames.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getTableHeadingRE(): RegExp {
  return RegExp(/(^[- ]+$)|(^[- +:]+$)|(^[- |:+]+$)/.source)
}

/**
 * Returns a regular expression that matches Markdown Tables.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getTableRE(): RegExp {
  return RegExp(/^\|.+\|$/.source, 'i')
}

/**
 * Returns a regular expression that matches tasks.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getTaskRE(): RegExp {
  return RegExp(/^(\s*)([-+*]) \[( |x)\] /.source, 'g')
}

/**
 * Returns a regular expression that matches URLs.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getUrlRE(): RegExp {
  return RegExp(
    /^\[([^\]]+)\]\((.+?)\)|(((?:(?:aaas?|about|acap|adiumxtra|af[ps]|aim|apt|attachment|aw|beshare|bitcoin|bolo|callto|cap|chrome(?:-extension)?|cid|coap|com-eventbrite-attendee|content|crid|cvs|data|dav|dict|dlna-(?:playcontainer|playsingle)|dns|doi|dtn|dvb|ed2k|facetime|feed|file|finger|fish|ftp|geo|gg|git|gizmoproject|go|gopher|gtalk|h323|hcp|https?|iax|icap|icon|im|imap|info|ipn|ipp|irc[6s]?|iris(?:\.beep|\.lwz|\.xpc|\.xpcs)?|itms|jar|javascript|jms|keyparc|lastfm|ldaps?|magnet|mailto|maps|market|message|mid|mms|ms-help|msnim|msrps?|mtqp|mumble|mupdate|mvn|news|nfs|nih?|nntp|notes|oid|opaquelocktoken|palm|paparazzi|platform|pop|pres|proxy|psyc|query|res(?:ource)?|rmi|rsync|rtmp|rtsp|secondlife|service|session|sftp|sgn|shttp|sieve|sips?|skype|sm[bs]|snmp|soap\.beeps?|soldat|spotify|ssh|steam|svn|tag|teamspeak|tel(?:net)?|tftp|things|thismessage|tip|tn3270|tv|udp|unreal|urn|ut2004|vemmi|ventrilo|view-source|webcal|wss?|wtai|wyciwyg|xcon(?:-userid)?|xfire|xmlrpc\.beeps?|xmpp|xri|ymsgr|z39\.50[rs]?):(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.-]+[.][a-z]{2,4}\/)(?:[^\s()<>]|\([^\s()<>]*\))+(?:\([^\s()<>]*\)|[^\s`*!()[\]{};:'".,<>?«»“”‘’])))|([a-z0-9.\-_+]+?@[a-z0-9.\-_+]+\.[a-z]{2,7})$/
      .source,
    'i'
  )
}

/**
 * Returns a regular expression that matches inline frames.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getWysiwygRE(): RegExp {
  return RegExp(/__(.+?)__|_(.+?)_|\*\*(.+?)\*\*|\*(.+?)\*|^(#{1,6}) (.+?)$|^(?:\s*)> (.+)$|`(.+?)`/.source, 'gi')
}

/**
 * Returns a regular expression that matches Zettelkasten IDs.
 *
 * @returns  {RegExp}              The compiled Regular Expression
 */
export function getZknTagRE(): RegExp {
  return RegExp(/##?[^\s,.:;…!?"'`»«“”‘’—–@$%&*#^+~÷\\/|<=>[\](){}]+#?/.source, 'i')
}
